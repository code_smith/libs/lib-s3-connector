package kz.codesmith.libs3connector.configuration;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Configuration
@Validated
@ConfigurationProperties(prefix = "s3-storage")
public class S3StorageConfigurationProperties {

  @NotBlank
  private String endpointUrl;
  @NotBlank
  private String presignedUrl;
  @NotBlank
  private String accessKey;
  @NotBlank
  private String secretKey;
  @NotBlank
  private String bucketName;
  private String region;
  @NotNull
  private Integer maxConnections;
  @NotNull
  private Integer connectionTimeout;
  @NotNull
  private Integer socketTimeout;

}


