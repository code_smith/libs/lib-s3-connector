package kz.codesmith.libs3connector.configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class S3ClientConfiguration {

  private final S3StorageConfigurationProperties s3Configuration;

  @Bean
  public AmazonS3 s3Client() {
    ClientConfiguration clientConfig = new ClientConfiguration();
    clientConfig.setMaxConnections(
        s3Configuration.getMaxConnections()); // Максимальное количество соединений
    clientConfig.setConnectionTimeout(
        s3Configuration.getConnectionTimeout()); // Таймаут соединения в миллисекундах
    clientConfig.setSocketTimeout(
        s3Configuration.getSocketTimeout()); // Таймаут сокета в миллисекундах
    BasicAWSCredentials awsCreds = new BasicAWSCredentials(
        s3Configuration.getAccessKey(),
        s3Configuration.getSecretKey()
    );
    return AmazonS3ClientBuilder.standard()
        .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
            s3Configuration.getEndpointUrl(), s3Configuration.getRegion()
        )).withClientConfiguration(clientConfig)
        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
        .withPathStyleAccessEnabled(true)
        .build();

  }

}
