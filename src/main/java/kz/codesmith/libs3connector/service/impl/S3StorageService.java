package kz.codesmith.libs3connector.service.impl;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kz.codesmith.libs3connector.model.PresignedUrlResponse;
import kz.codesmith.libs3connector.service.StorageService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3StorageService implements StorageService {

  private final AmazonS3 s3client;
  @Value("${s3-storage.bucket-name}")
  private String bucketName;
  @Value("${s3-storage.presigned-url}")
  private String presignedUrlEndpoint;

  @Override
  public String put(
      String key,
      InputStream content,
      Long contentLength,
      String mediaType,
      String username
  ) {
    log.info("Put to {} file {}", bucketName, key);

    var metadata = new ObjectMetadata();
    metadata.setContentType(mediaType + "; charset=utf-8");
    metadata.setLastModified(new Date());
    metadata.setCacheControl("private, max-age=86400");
    metadata.setContentLength(contentLength);
    if (Objects.nonNull(username) && !username.isEmpty()) {
      metadata.setUserMetadata(Map.of("username", username));
    }
    var uploadReq = new PutObjectRequest(bucketName, key, content, metadata);
    s3client.putObject(uploadReq);
    var url = s3client.getUrl(bucketName, key).toExternalForm();
    log.info("File uploaded to {} file {}, S3 url {}", bucketName, key, url);
    return url;
  }

  @Override
  public void delete(String key) {
    log.info("Delete file: {}, bucket name: {}", key, bucketName);

    s3client.deleteObject(bucketName, key);
    log.info("File deleted");
  }

  @Override
  public S3Object get(String key) {
    log.info("Get from {} file {}", bucketName, key);
    return s3client.getObject(
        bucketName,
        key
    );
  }

  @Override
  @SneakyThrows
  public PresignedUrlResponse generatePresignedUrl(
      String fileKey,
      String filename,
      Duration expiration,
      boolean asAttachment
  ) {
    log.info("Generate presigned url, bucket name: {} file {}", bucketName, fileKey);

    GeneratePresignedUrlRequest generatePresignedUrlRequest =
        new GeneratePresignedUrlRequest(bucketName, fileKey)
            .withMethod(HttpMethod.GET)
            .withExpiration(getDateFromDuration(expiration));

    if (asAttachment) {
      generatePresignedUrlRequest.addRequestParameter("response-content-disposition",
          "attachment; filename*=UTF-8''\"" + filename + "\"");
    } else {
      generatePresignedUrlRequest.addRequestParameter("response-content-disposition",
          "inline; filename*=UTF-8''\"" + filename + "\"");
    }

    var presignedUrl = s3client.generatePresignedUrl(generatePresignedUrlRequest);

    // Замените хост на кастомный
    var endpoint = presignedUrl.getProtocol() + "://" + presignedUrl.getAuthority();
    if (!endpoint.startsWith(presignedUrlEndpoint)) {
      var presignedUrlString = presignedUrl.toString()
          .replace(endpoint, presignedUrlEndpoint);
      return new PresignedUrlResponse(
          filename,
          fileKey,
          URI.create(presignedUrlString).toURL().toString()
      );
    }
    return new PresignedUrlResponse(filename, fileKey, presignedUrl.toString());
  }

  @Override
  public List<PresignedUrlResponse> generateUploadPresignedUrls(
      String objectId,
      Duration expiration,
      List<String> fileNames
  ) {
    return fileNames.stream().map(fileName -> {
      String fileKey = objectId + "/" + fileName;

      GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, fileKey)
          .withMethod(HttpMethod.PUT)
          .withExpiration(getDateFromDuration(expiration));

      URL url = s3client.generatePresignedUrl(request);

      return new PresignedUrlResponse(fileName, fileKey, url.toString());
    }).toList();
  }

  private Date getDateFromDuration(Duration expiration) {
    // Get the current instant
    Instant now = Instant.now();

    // Add the duration to the current instant
    Instant futureInstant = now.plus(expiration);

    // Convert Instant to Date

    return Date.from(futureInstant);
  }
}