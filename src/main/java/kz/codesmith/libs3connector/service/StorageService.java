package kz.codesmith.libs3connector.service;

import com.amazonaws.services.s3.model.S3Object;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.io.InputStream;
import java.time.Duration;
import java.util.List;
import kz.codesmith.libs3connector.model.PresignedUrlResponse;

public interface StorageService {

  String put(
      @NotBlank String key,
      @NotNull InputStream content,
      @NotNull Long contentLength,
      @NotBlank String mediaType,
      @NotBlank String username
  );

  void delete(@NotBlank String key);

  S3Object get(@NotBlank String key);

  PresignedUrlResponse generatePresignedUrl(
      @NotBlank String fileKey,
      @NotBlank String filename,
      @NotNull Duration expiration,
      boolean asAttachment
  );

  List<PresignedUrlResponse> generateUploadPresignedUrls(
      @NotBlank String objectId,
      @NotNull Duration expiration,
      @NotEmpty List<@NotBlank String> fileNames
  );
}
