package kz.codesmith.libs3connector.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Data
public class PresignedUrlResponse {
  private String filename;
  private String fileKey;
  private String url;
}
